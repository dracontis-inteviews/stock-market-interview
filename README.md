# Assignment – Software Engineer

## Application

Create a JVM based backend application using REST. That contains the following endpoints:

* GET /api/stocks (get a list of stocks)
* POST /api/stocks (create a stock)
* GET /api/stocks/1 (get one stock from the list)
* PUT /api/stocks/1 (update the price of a single stock)
* DELETE/api/stocks/1 (delete a single stock)

The initial list of stocks should be created **in-memory** on application **start-up**. The stock object contains at
least the following fields:

1. id (Number);
2. name (String);
3. currentPrice (Amount);
4. lastUpdate (Timestamp).

Each stock must be protected from abusive price updates, thus it must be **locked** for a **minimum** of 5 minutes
before it can be updated or deleted. Each stock must be **automatically unlocked** at the time when its lock expires (a
state desynchronization of maximum of 5 seconds after expiration is allowed).

Each endpoint must be compliant with the [HTTP/1.1](https://datatracker.ietf.org/doc/html/rfc2616)
and [REST](https://www.w3.org/2001/sw/wiki/REST) standards.

Use any frameworks that you see fit to build and test this application.

## Implementation

Treat this application as a real MVP that should go to production. All main use cases must be covered by at least one
unit or(and) integration test.