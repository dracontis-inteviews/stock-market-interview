package com.stock.interview.service

import com.stock.interview.config.Metric
import com.stock.interview.config.exception.EntityDuplicationException
import com.stock.interview.config.exception.EntityIsLockedException
import com.stock.interview.config.exception.EntityNotFoundException
import com.stock.interview.model.Stock
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.Timer
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.DelayQueue
import java.util.concurrent.atomic.AtomicLong
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import kotlin.concurrent.timerTask
import kotlin.streams.toList

interface StocksService {
    fun getAllStocks(): Collection<Stock>
    fun findById(id: Long): Stock
    fun deleteById(id: Long): Boolean
    fun create(stock: Stock): Stock
    fun updatePrice(id: Long, newPrice: BigDecimal): Stock
}

@Service
class StocksServiceImpl(
    private val delayInSeconds: Long = 5 * 60L
) : StocksService {

    private lateinit var meterRegistry: MeterRegistry

    private val lockedStocks = DelayQueue<DelayedStock>()
    private val stocks = ConcurrentLinkedQueue<Stock>()
    private val idSequence = AtomicLong(0)
    private val timer = Timer()

    @PostConstruct
    fun setUnlockingTimer() {
        val task = timerTask {
            meterRegistry.timer(Metric.UNLOCKER_SCHEDULER_TIME.name).record {
                do {
                    val delayedStock = lockedStocks.poll()
                    delayedStock?.stock?.let {
                        it.isLocked = false
                        it.lastUpdated = LocalDateTime.now()
                    }
                } while (delayedStock != null)
            }
        }
        timer.scheduleAtFixedRate(task, 0, 1_000)
    }

    @PreDestroy
    fun cancelTimer() {
        timer.cancel()
    }

    override fun getAllStocks() = stocks.stream().toList()

    override fun findById(id: Long): Stock = stocks.stream().filter { it.id == id }
        .findFirst().orElseThrow { EntityNotFoundException("Entity with id $id not found") }

    override fun deleteById(id: Long): Boolean {
        val stock = findById(id)
        if (stock.isLocked) {
            throw EntityIsLockedException("Can't delete entity right now")
        }
        return stocks.remove(stock)
    }

    override fun create(stock: Stock): Stock {
        if (stocks.stream().anyMatch { it.name == stock.name }) {
            throw EntityDuplicationException("Stock is already exists")
        }

        stock.id = idSequence.incrementAndGet()
        lockedStocks.add(DelayedStock(stock, delayInSeconds))
        stocks.add(stock)
        return stock
    }

    override fun updatePrice(id: Long, newPrice: BigDecimal): Stock {
        val stock = findById(id)
        if (stock.isLocked) {
            throw EntityIsLockedException("Can't update entity right now")
        }

        stock.lastUpdated = LocalDateTime.now()
        stock.currentPrice = newPrice

        val oldStock = stocks.stream().filter { it.id == stock.id }
            .findFirst().orElseThrow { EntityNotFoundException("Entity with id ${stock.id} not found") }
        stocks.remove(oldStock)
        stocks.add(stock)
        return stock
    }

    @Autowired
    fun setMeterRegistry(meterRegistry: MeterRegistry) {
        this.meterRegistry = meterRegistry
    }
}