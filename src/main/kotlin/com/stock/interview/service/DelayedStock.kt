package com.stock.interview.service

import com.stock.interview.model.Stock
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.Delayed
import java.util.concurrent.TimeUnit


const val DELAY_SECONDS = 5 * 60L

data class DelayedStock(
    val stock: Stock,
    val delayInSeconds: Long = DELAY_SECONDS
) : Delayed {

    override fun getDelay(unit: TimeUnit): Long {
        val diff =
            convertToEpochSeconds(stock.createdAt.plusSeconds(delayInSeconds)) - convertToEpochSeconds(LocalDateTime.now())
        return unit.convert(diff, TimeUnit.MILLISECONDS)
    }

    override fun compareTo(other: Delayed) =
        (convertToEpochSeconds(this.stock.createdAt) -
                convertToEpochSeconds((other as DelayedStock).stock.createdAt)).toInt()

    private fun convertToEpochSeconds(dateTime: LocalDateTime) = dateTime.toInstant(ZoneOffset.UTC).epochSecond
}