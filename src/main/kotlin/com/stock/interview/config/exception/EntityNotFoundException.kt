package com.stock.interview.config.exception

class EntityNotFoundException(override val message: String) : RuntimeException(message)