package com.stock.interview.config.exception

class EntityDuplicationException(override val message: String) : RuntimeException(message)