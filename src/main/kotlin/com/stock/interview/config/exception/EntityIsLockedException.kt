package com.stock.interview.config.exception

class EntityIsLockedException(override val message: String) : RuntimeException(message)