package com.stock.interview.config

import com.stock.interview.config.exception.EntityDuplicationException
import com.stock.interview.config.exception.EntityIsLockedException
import com.stock.interview.config.exception.EntityNotFoundException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.function.Consumer
import javax.validation.ConstraintViolationException

@ControllerAdvice
class ControllerAdviceConfig : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [EntityDuplicationException::class])
    fun handleEntityDuplicationException(ex: EntityDuplicationException, request: WebRequest) =
        ResponseEntity.badRequest().body(ex.message)

    @ExceptionHandler(value = [EntityIsLockedException::class])
    fun handleEntityIsLockedException(ex: EntityIsLockedException, request: WebRequest) =
        ResponseEntity.internalServerError().body(ex.message)

    @ExceptionHandler(value = [EntityNotFoundException::class])
    fun handleEntityNotFoundException(ex: EntityNotFoundException, request: WebRequest) =
        ResponseEntity.notFound()

    @ExceptionHandler(value = [ConstraintViolationException::class])
    fun handleConstraintViolationException(ex: ConstraintViolationException, request: WebRequest) =
        ResponseEntity.badRequest().body(ex.message)

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val errors: MutableMap<String, String?> = HashMap()
        ex.bindingResult.allErrors.forEach(Consumer { error: ObjectError ->
            val fieldName = (error as FieldError).field
            val errorMessage = error.getDefaultMessage()
            errors[fieldName] = errorMessage
        })
        return ResponseEntity.badRequest().body(errors)
    }
}