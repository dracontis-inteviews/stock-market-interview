package com.stock.interview.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor
import javax.validation.Validator

@Configuration
class ValidationConfig {

    @Bean
    fun validator(): Validator = LocalValidatorFactoryBean()

    @Bean
    fun methodValidationPostProcessor(validator: Validator): MethodValidationPostProcessor =
        MethodValidationPostProcessor().apply { this.setValidator(validator) }
}