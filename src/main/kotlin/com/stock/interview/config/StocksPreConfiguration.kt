package com.stock.interview.config

import com.stock.interview.model.Stock
import com.stock.interview.service.StocksService
import org.springframework.context.annotation.Configuration
import java.math.BigDecimal
import java.util.Random
import java.util.UUID
import javax.annotation.PostConstruct

@Configuration
class StocksPreConfiguration(
    private val stocksService: StocksService
) {

    @PostConstruct
    fun initStocks() {
        repeat(10) {
            stocksService.create(
                Stock(
                    name = UUID.randomUUID().toString(),
                    currentPrice = BigDecimal.valueOf(Random().nextDouble())
                )
            )
        }
    }
}