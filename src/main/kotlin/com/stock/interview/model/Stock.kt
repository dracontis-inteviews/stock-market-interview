package com.stock.interview.model

import java.math.BigDecimal
import java.time.LocalDateTime

data class Stock(
    var id: Long = 0,
    var name: String,
    var currentPrice: BigDecimal,
    var isLocked: Boolean = true,
    val createdAt: LocalDateTime = LocalDateTime.now(),
    var lastUpdated: LocalDateTime = LocalDateTime.now()
)