package com.stock.interview.controller

import com.stock.interview.model.Stock
import com.stock.interview.service.StocksService
import mu.KotlinLogging
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

data class CreateStockRequest(
    @field:NotBlank
    var name: String,
    @field:Min(0)
    var currentPrice: BigDecimal
)

data class UpdatePriceRequest(
    @field:Min(0)
    val currentPrice: BigDecimal
)

@Validated
@RestController
@RequestMapping("/api/stocks")
class StocksController(
    private val stocksService: StocksService
) {

    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getAllStocks(): Collection<Stock> {
        logger.info { "--> Received" }
        return stocksService.getAllStocks().also {
            logger.info { "<-- Processed: ${it.size}" }
        }
    }

    @PostMapping
    fun create(@Valid @RequestBody request: CreateStockRequest): Stock {
        logger.info { "--> Received with payload: $request" }
        return stocksService.create(
            Stock(
                name = request.name,
                currentPrice = request.currentPrice
            )
        ).also {
            logger.info { "<-- Processed: ${it.id}" }
        }
    }

    @GetMapping("/{id}")
    fun getById(@Min(1) @PathVariable id: Long): Stock {
        logger.info { "--> Received with id $id" }
        return stocksService.findById(id).also {
            logger.info { "<-- Processed" }
        }
    }

    @PutMapping("/{id}")
    fun updatePrice(@Min(1) @PathVariable id: Long, @Valid @RequestBody request: UpdatePriceRequest): Stock {
        logger.info { "--> Received with id $id and payload: $request" }
        return stocksService.updatePrice(id, request.currentPrice).also {
            logger.info { "<-- Processed, new price is ${it.currentPrice}" }
        }
    }

    @DeleteMapping("/{id}")
    fun delete(@Min(1) @PathVariable id: Long) {
        logger.info { "--> Received with id $id" }
        stocksService.deleteById(id).also {
            logger.info { "<-- Processed with result: $it" }
        }
    }
}