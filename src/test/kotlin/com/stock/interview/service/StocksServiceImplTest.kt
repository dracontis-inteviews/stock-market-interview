package com.stock.interview.service

import com.stock.interview.config.Metric
import com.stock.interview.config.exception.EntityDuplicationException
import com.stock.interview.config.exception.EntityIsLockedException
import com.stock.interview.config.exception.EntityNotFoundException
import com.stock.interview.model.Stock
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.greaterThanOrEqualTo
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import java.math.BigDecimal
import java.util.Random
import java.util.UUID
import org.hamcrest.core.Is.`is` as Is

private const val TEST_DELAY_SECONDS = 2L

internal class StocksServiceImplTest {

    private val meterRegistry = SimpleMeterRegistry()
    private val service = StocksServiceImpl(TEST_DELAY_SECONDS).apply {
        setMeterRegistry(meterRegistry)
    }

    @Test
    fun `verify unblocking logic`() {
        service.setUnlockingTimer()
        val stock = addStock()

        assertThat(stock.isLocked, Is(true))
        Thread.sleep((TEST_DELAY_SECONDS + 1) * 1_000)
        assertThat(stock.isLocked, Is(false))

        service.cancelTimer()
        assertThat(meterRegistry.timer(Metric.UNLOCKER_SCHEDULER_TIME.name).count(), Is(greaterThanOrEqualTo(1)))
    }

    @Test
    fun `get all stocks executed successfully`() {
        addStock()

        val allStocks = service.getAllStocks()

        assertThat(allStocks.size, Is(greaterThanOrEqualTo(1)))
    }

    @Test
    fun `find by id executed successfully`() {
        val stock = addStock()

        val found = service.findById(stock.id)

        Assertions.assertAll("Stock not found",
            Executable { assertThat(found, Is(equalTo(stock))) }
        )
    }

    @Test
    fun `not found exception on find by id`() {
        Assertions.assertThrows(EntityNotFoundException::class.java) {
            service.findById(Long.MAX_VALUE)
        }
    }

    @Test
    fun `create executed successfully`() {
        val result = addStock()

        Assertions.assertAll(
            "Save was unsuccessful",
            Executable { assertThat(result.id, Is(not(equalTo(result.createdAt)))) },
            Executable { assertThat(result.currentPrice, Is(equalTo(result.currentPrice))) },
            Executable { assertThat(result.name, Is(equalTo(result.name))) }
        )
    }

    @Test
    fun `duplication exception on two create calls with same name`() {
        val result = addStock()
        result.id = 0

        Assertions.assertThrows(EntityDuplicationException::class.java) {
            service.create(result)
        }
    }

    @Test
    fun `update executed successfully`() {
        val stock = addStock(isLocked = false)
        val priceBeforeUpdate = stock.currentPrice
        stock.currentPrice = BigDecimal.TEN

        val updatedStock = service.updatePrice(stock.id, BigDecimal.TEN)

        assertThat(updatedStock.currentPrice, Is(equalTo(BigDecimal.TEN)))
        assertThat(updatedStock.currentPrice, Is(not(equalTo(priceBeforeUpdate))))
    }

    @Test
    fun `lock exception on update`() {
        val stock = addStock()

        Assertions.assertThrows(EntityIsLockedException::class.java) {
            service.updatePrice(stock.id, BigDecimal.TEN)
        }
    }

    @Test
    fun `delete by id executed successfully`() {
        val stock = addStock(isLocked = false)

        val result = service.deleteById(stock.id)

        assertThat(result, Is(true))
    }

    @Test
    fun `lock exception on delete`() {
        val stock = addStock()

        Assertions.assertThrows(EntityIsLockedException::class.java) {
            service.deleteById(stock.id)
        }
    }

    @Test
    fun `entity not found on delete`() {
        Assertions.assertThrows(EntityNotFoundException::class.java) {
            service.deleteById(Long.MAX_VALUE)
        }
    }

    private fun addStock(isLocked: Boolean = true) = service.create(
        Stock(
            name = UUID.randomUUID().toString(),
            currentPrice = BigDecimal.valueOf(Random().nextDouble()),
            isLocked = isLocked
        )
    )
}
