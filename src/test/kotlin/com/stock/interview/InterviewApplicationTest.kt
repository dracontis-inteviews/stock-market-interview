package com.stock.interview

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import javax.annotation.PostConstruct
import org.hamcrest.core.Is.`is` as Is


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
internal class InterviewApplicationTest {

    companion object {
        private const val HEALTH = "actuator/health"
        private const val PROMETHEUS = "actuator/prometheus"
    }

    @LocalServerPort
    private var port: Int = 0
    private lateinit var url: String

    @Autowired
    private lateinit var testRestTemplate: TestRestTemplate

    @PostConstruct
    private fun init() {
        url = "http://localhost:$port"
    }

    @Test
    fun `verify health check endpoint`() {
        val result = testRestTemplate.getForEntity("$url/$HEALTH", String::class.java)

        assertThat(result.statusCode, Is(equalTo(HttpStatus.OK)))
        assertThat(result.body, Is(containsString("\"status\":\"UP\"")))
    }

}