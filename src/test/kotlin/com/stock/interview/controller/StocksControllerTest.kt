package com.stock.interview.controller

import com.stock.interview.config.exception.EntityDuplicationException
import com.stock.interview.config.exception.EntityIsLockedException
import com.stock.interview.config.exception.EntityNotFoundException
import com.stock.interview.model.Stock
import com.stock.interview.service.StocksServiceImpl
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import java.math.BigDecimal
import java.util.Random
import java.util.UUID
import javax.annotation.PostConstruct
import org.hamcrest.core.Is.`is` as Is


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
internal class StocksControllerTest {

    @LocalServerPort
    private var port: Int = 0
    private lateinit var url: String

    @MockBean
    private lateinit var stockService: StocksServiceImpl

    @Autowired
    private lateinit var testRestTemplate: TestRestTemplate

    @PostConstruct
    private fun init() {
        url = "http://localhost:$port/api/stocks"
    }

    @Test
    fun `get all stocks executed successfully`() {
        val stock = createStock()
        `when`(stockService.getAllStocks()).thenReturn(listOf(stock, createStock()))

        val response = testRestTemplate.getForEntity(url, Array<out Stock>::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
        assertThat(response.body?.size, Is(equalTo(2)))
        assertThat(response.body?.get(0)?.id, Is(equalTo(stock.id)))
    }

    @Test
    fun `get all stocks returned empty array of container is empty`() {
        `when`(stockService.getAllStocks()).thenReturn(emptyList())

        val response = testRestTemplate.getForEntity(url, Array<out Stock>::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
        assertThat(response.body?.size, Is(equalTo(0)))
    }

    @Test
    fun `create executed successfully`() {
        val request = CreateStockRequest(
            name = UUID.randomUUID().toString(),
            currentPrice = BigDecimal.valueOf(Random().nextDouble())
        )
        `when`(stockService.create(anyObject())).thenReturn(
            Stock(
                name = request.name,
                currentPrice = request.currentPrice
            )
        )

        val response = testRestTemplate.postForEntity(url, request, Stock::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
        assertAll("Create failed",
            Executable { assertThat(response.body?.name, Is(equalTo(request.name))) },
            Executable { assertThat(response.body?.currentPrice, Is(equalTo(request.currentPrice))) }
        )
    }

    @Test
    fun `create failed on request validation`() {
        val request = CreateStockRequest(
            name = "   ",
            currentPrice = BigDecimal.valueOf(-10)
        )

        val response = testRestTemplate.postForEntity(url, request, Map::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body?.get("name"), Is(equalTo("must not be blank")))
        assertThat(response.body?.get("currentPrice"), Is(equalTo("must be greater than or equal to 0")))
    }

    @Test
    fun `create failed on duplication`() {
        val request = CreateStockRequest(
            name = UUID.randomUUID().toString(),
            currentPrice = BigDecimal.valueOf(Random().nextDouble())
        )
        `when`(stockService.create(anyObject())).thenThrow(EntityDuplicationException("Stock is already exists"))

        val response = testRestTemplate.postForEntity(url, request, String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body, Is(equalTo("Stock is already exists")))
    }

    @Test
    fun `find by id executed successfully`() {
        `when`(stockService.findById(1)).thenReturn(createStock(1))

        val response = testRestTemplate.getForEntity("$url/1", Stock::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
        assertThat(response.body?.id, Is(equalTo(1)))
    }

    @Test
    fun `find by id failed on missing entity`() {
        `when`(stockService.findById(1)).thenThrow(EntityNotFoundException("Entity with id 1 not found"))

        val response = testRestTemplate.getForEntity("$url/1", Unit::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.NOT_FOUND)))
    }

    @Test
    fun `find by id failed on validation`() {
        val response = testRestTemplate.getForEntity("$url/-1", String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body, Is(equalTo("getById.id: must be greater than or equal to 1")))
    }

    @Test
    fun `update price executed successfully`() {
        val request = UpdatePriceRequest(
            currentPrice = BigDecimal.TEN
        )
        `when`(stockService.updatePrice(1L, BigDecimal.TEN)).thenReturn(createStock().apply {
            currentPrice = BigDecimal.TEN
        })

        val response = testRestTemplate.exchange("$url/1", HttpMethod.PUT, HttpEntity(request), Stock::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
        assertThat(response.body?.currentPrice, Is(equalTo(BigDecimal.TEN)))
    }

    @Test
    fun `update price failed on missing entity`() {
        val request = UpdatePriceRequest(
            currentPrice = BigDecimal.TEN
        )
        `when`(
            stockService.updatePrice(
                1L,
                BigDecimal.TEN
            )
        ).thenThrow(EntityNotFoundException("Entity with id 1 not found"))

        val response = testRestTemplate.exchange("$url/1", HttpMethod.PUT, HttpEntity(request), Unit::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.NOT_FOUND)))
    }

    @Test
    fun `update price failed on locked state`() {
        val request = UpdatePriceRequest(
            currentPrice = BigDecimal.TEN
        )
        `when`(
            stockService.updatePrice(
                1L,
                BigDecimal.TEN
            )
        ).thenThrow(EntityIsLockedException("Can't update entity right now"))

        val response = testRestTemplate.exchange("$url/1", HttpMethod.PUT, HttpEntity(request), String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.INTERNAL_SERVER_ERROR)))
        assertThat(response.body, Is(equalTo("Can't update entity right now")))

    }

    @Test
    fun `update price failed on invalid id`() {
        val request = UpdatePriceRequest(
            currentPrice = BigDecimal.TEN
        )

        val response = testRestTemplate.exchange("$url/-1", HttpMethod.PUT, HttpEntity(request), String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body, Is(equalTo("updatePrice.id: must be greater than or equal to 1")))
    }

    @Test
    fun `update price failed on invalid price`() {
        val request = UpdatePriceRequest(
            currentPrice = -BigDecimal.TEN
        )

        val response = testRestTemplate.exchange("$url/1", HttpMethod.PUT, HttpEntity(request), Map::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body?.get("currentPrice"), Is(equalTo("must be greater than or equal to 0")))
    }

    @Test
    fun `delete executed successfully`() {
        val response = testRestTemplate.exchange("$url/1", HttpMethod.DELETE, null, Unit::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.OK)))
    }

    @Test
    fun `delete failed on missing entity`() {
        `when`(stockService.deleteById(1L)).thenThrow(EntityNotFoundException("Entity with id 1 not found"))

        val response = testRestTemplate.exchange("$url/1", HttpMethod.DELETE, null, Unit::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.NOT_FOUND)))
    }

    @Test
    fun `delete failed on locked state`() {
        `when`(stockService.deleteById(1L)).thenThrow(EntityIsLockedException("Can't delete entity right now"))

        val response = testRestTemplate.exchange("$url/1", HttpMethod.DELETE, null, String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.INTERNAL_SERVER_ERROR)))
        assertThat(response.body, Is(equalTo("Can't delete entity right now")))
    }

    @Test
    fun `delete failed on invalid id`() {
        val request = UpdatePriceRequest(
            currentPrice = BigDecimal.TEN
        )

        val response = testRestTemplate.exchange("$url/-1", HttpMethod.DELETE, HttpEntity(request), String::class.java)

        assertThat(response.statusCode, Is(equalTo(HttpStatus.BAD_REQUEST)))
        assertThat(response.body, Is(equalTo("delete.id: must be greater than or equal to 1")))
    }

    private fun createStock(id: Long? = null) = Stock(
        id = id ?: Random().nextLong(),
        name = UUID.randomUUID().toString(),
        currentPrice = BigDecimal.valueOf(Random().nextDouble())
    )

    private fun <T> anyObject(): T {
        return Mockito.any<T>()
    }

}